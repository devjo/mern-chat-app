const groups = [
    { name: 'Dance' },
    { name: 'Education' },
    { name: 'Jobs' },
    { name: 'Sports' },
    { name: 'Gists' }
];

const getGroup = (groupName) => groups.find(name => groupName === name);

module.exports = { getGroup };